<?php
require(__DIR__.'/../../vendor/autoload.php');

use F2\Type;
use function F2\asserty;

function length_validator($value, $type): bool {
    if (strlen($value) > 10) {
        return false;
    }
    if (strlen($value) < 5) {
        return false;
    }
    return true;
}

$type = Type\STRING + [ '#validator' => 'length_validator' ];

for ($length = 0; $length < 5; $length++) {
    $string = str_pad('', $length, '-');
    asserty( $length < 5 || $length > 10 != Type\is($string, $type) );
}


