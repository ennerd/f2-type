<?php
require(__DIR__.'/../../vendor/autoload.php');

use F2\Type;
use function F2\asserty;

$res = fopen(__FILE__, 'r');
$obj = (object) [ 'an' => 'object' ];

/*
    0,
    0.0,
    0.5,
    "0",
    "0.0",
    "0.5",
    1,
    1.0,
    1.5,
    "1",
    "1.0",
    "1.5",
    "",
    "false",
    "true",
    "0 string",
    "not a number",
    "a_callable",
    [ Foo::class, 'bar' ], 
    [ Foo::class, 'not-a-method' ],
    $res,
    $obj,
*/
//                          0       1       2       3       4       5       6       7       8       9       10      11      12     13       14      15      16      17      18      19
$values = [
                            false,  true,    -1,    0,      1,      -1.0,   0.0,    1.0,    "-1",   "0",    "1",    "-1.0", "0.0",  "1.0",  "Hi!",  "",     null,   [],     [1],    ["frode"],  fopen(__FILE__, 'r'), 'Foo', ['Foo', 'bar'], new Foo(), Type\INT, 
];

$rules = [
    [
    Type\BOOL::class,
    function($v) { return (bool) $v; },
    ],[
    Type\INT::class,
    function($v) { return (int) $v; },
    [ 23 => null, ]
    ],[
    Type\FLOAT::class,
    function($v) { $result = (float) $v; return $result; },
    [ 23 => null, ],
    ],[
    Type\NUMBER::class,
    function($v) { if ( (float) $v == (int) $v ) return (int) $v; return (float) $v; },
    [ 23 => null, ]
    ],[
    Type\STRING::class,
    function($v) { return (string) $v; },
    [17 => null, 18 => null, 19 => null, 22 => null, 23 => null, 24 => null, ],
    ],[
    Type\ARR::class,
    function($v) { return (array) $v; },
    ],[
    Type\OBJ::class,
    function ($v) { return (object) $v; },
    ],[
    TYPE\CALLBACK::class,
    function($v) { return is_callable($v) ? $v : null; },
    ],[
    Type\RESOURCE::class,
    function($v) { return is_resource($v) ? $v : null; },
    ],[
    Type\NUL::class,
    function($v) { return null; },
    ],[
    Type\TYPE::class,
    function($v) { return null; },
    [ 24 => Type\INT ],
    ],[
    Type\CLASSNAME::class,
    function($v) { return null; },
    [ 21 => Foo::class ],
    ],[
    Type\MIXED::class,
    function($v) { return $v; },
    ]
];

foreach ($rules as $rule) {

    $tCaster = $rule[0];
    $pCaster = $rule[1];

    foreach ($values as $i => $value) {
        $castedTo = $tCaster($value);
        if (isset($rule[2]) && array_key_exists($i, $rule[2])) {
            $expectedTo = $rule[2][$i];
        } else {
            $expectedTo = $pCaster($value, $castedTo);
        }
        if (is_object($castedTo) && is_object($expectedTo) && serialize($castedTo) === serialize($expectedTo)) {
            $result = true;
        } else {
            $result = $castedTo === $expectedTo;
        }
        asserty( $result, "Failed: $tCaster(".short_dump($value).") !== ".short_dump($expectedTo) );
        if ($result === false) {
            
            echo $i.". ".short_dump($value)." ".short_dump($castedTo)." === ".short_dump($expectedTo)." ".short_dump($result)."\n";
        }
    }
}

function shorten(string $string) {
    if (strlen($string) > 50) {
        return substr($string, 0, 17)."...";
    }
    return $string;
}

function short_dump($mixed) {
    if (is_array($mixed)) {
        return json_encode($mixed);
    }
    if (is_object($mixed)) return get_class($mixed).":".serialize($mixed);
    if (is_bool($mixed)) return json_encode($mixed);
    return gettype($mixed).'('.shorten(var_export($mixed, true)).')';
}

class Foo {
    public static function bar() {}
}
