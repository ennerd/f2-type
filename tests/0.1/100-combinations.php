<?php
require(__DIR__.'/../../vendor/autoload.php');

use F2\Type;
use function F2\{asserty, expect};

$fp = fopen(__FILE__, 'r');
$obj = new Foo();
$cb = function() {};

$testCases = [
    // Type to legal values mapping. Values for other types are assumed to be illegal, unless also specified

    [ Type\INT, [-1, 0, 10, 1000, 10000], ],
    [ Type\FLOAT, [ -1.0, 0.0, 10.0, 100.5, 10500.99, 2/3 ], ],
    [ Type\STRING, ['string', 'longer string', 'bar', 'Foo', 'F2\Type\Foo'], ],
    [ Type\RESOURCE, [$fp], ],
    [ Type\NUL, [null], ],
    [ Type\BOOL, [true, false], ],
    [ Type\ARR, [['array'], Type\INT, Type\FLOAT, Type\STRING, Type\RESOURCE, Type\NUL, Type\BOOL, Type\ARR, Type\OBJ, Type\CALLBACK, Type\CLASSNAME, Type\MIXED, (array) $obj ] ],
    [ Type\OBJ, [$cb, $obj] ],
    [ Type\CALLBACK,  [$cb, 'bar'] ],
    [ Type\CLASSNAME, ['Foo'] ],
    [ Type\MIXED, [ -1, 0, 10, 1000, 10000, -1.0, 0.0, 10.0, 100.5, 10500.99, 2/3, 'string', 'longer string', 'bar', $fp, null, true, false, ['array'], Type\INT, Type\FLOAT, Type\STRING, Type\RESOURCE, Type\NUL, Type\BOOL, Type\ARR, Type\OBJ, Type\CALLBACK, Type\CLASSNAME, Type\MIXED, (array) $obj, $cb, $obj, $cb, 'bar', 'Foo', 'F2\Type\Foo', ] ],
];

$legalMap = [];
$typesToTest = [];
$valuesToTest = [];
foreach ( $testCases as $testCase ) {
    $typeHash = anyHash($testCase[0]);

    foreach ($testCase[1] as $legalValue) {
        $valueHash = anyHash($legalValue);

/*
if ($legalValue === null) {
        echo "Hash: ".short_dump($legalValue)."=".$valueHash."\n";
die();
}
*/
        if (array_key_exists($typeHash, $legalMap) && array_key_exists($valueHash, $legalMap[$typeHash]) && $legalMap[$typeHash][$valueHash] !== $legalValue) {
//        if (isset($legalMap[$typeHash][$valueHash]) && $legalMap[$typeHash][$valueHash] !== $legalValue) {
            asserty( false, 'Conflicting hash! Something is wrong with the test code');
            die("CONFLICTING HASH!");
        }
        $legalMap[$typeHash][$valueHash] = $legalValue;
    }

    $typesToTest[$typeHash] = $testCase[0];

    foreach ($testCase[1] as $value) {
        $valuesToTest[anyHash($value)] = $value;
    }
}

//var_dump($legalMap);
/*
die();
*/
foreach ($typesToTest as $typeHash => $type) {
//    echo "Testing type ".$type['#type'].":\n";
    foreach ($valuesToTest as $valueTypeHash => $value) {

//var_dump($value, $typeHash, anyHash($value));
        $legal = array_key_exists(anyHash($value), $legalMap[$typeHash]);
//        $legal = isset($legalMap[$typeHash][anyHash($value)]);

//        echo "- testing if ".short_dump($legal)." === Type\is(".shorten(gettype($value)."(".json_encode($value).")").", ".$type['#type'].")  valueHash = $valueHash\n";

        asserty( $legal === Type\is( $value, $type ) );

    }

}

class Foo {
    public static function staticFunction() {}
    public function nonStaticFunction() {}
}

function bar() {
}


function anyHash($value) {
    if ($value === null) {
        return 'nullValueHash';
    }
    //echo "anyHash: ".short_dump($value)."\n";
    if (is_resource($value)) {
        //echo "- resource\n";
        $value = (string) $value;
    } elseif (is_object($value)) {
        //echo "- spl_object_hash\n";
        $value = get_class($value).spl_object_hash($value);
    } else {
        //echo "- serialize\n";
        $value = serialize($value);
    }
    $hash = md5($value);
    return md5($value);
}

function shorten(string $string) {
    if (strlen($string) > 30) {
        return substr($string, 0, 17)."...";
    }
    return $string;
}

function short_dump($mixed) {
    if (is_array($mixed)) $mixed = json_encode($mixed);
    if (is_object($mixed)) $mixed = get_class($mixed).":".spl_object_hash($mixed);
    return gettype($mixed).'('.shorten(var_export($mixed, true)).')';
}
