<?php
require(__DIR__.'/../../vendor/autoload.php');

use F2\Type;
use function F2\{asserty, expect};

/**
 * Check that null is of type TYPE + NUL
 */
asserty( Type\is(null, Type\TYPE + Type\NUL) );

/**
 * Check if all types are of type TYPE
 */

foreach ( [
    Type\BOOL,
    Type\INT,
    Type\FLOAT,
    Type\STRING,
    Type\ARR,
    Type\OBJ,
    Type\CALLBACK,
    Type\RESOURCE,
    Type\NUL,
    Type\MIXED,
    Type\TYPE,
    Type\CLASSNAME,
] as $value) {
    asserty (Type\is($value, Type\TYPE));
}

/**
 * Check that all other types are NOT of type TYPE
 */
foreach ( [
    1, 
    1.5, 
    'string', 
    fopen(__FILE__, 'r'), 
    null,
    true,
    false,
    ['array'], 
    (object) ['prop'=>'val'], 
    function() {},
    'Foo',
    'bar',
] as $value) {
    asserty( !Type\is($value, Type\TYPE) );
}


class Foo {
}

function bar() {
}
