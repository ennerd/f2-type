<?php
require(__DIR__.'/../../vendor/autoload.php');

use F2\Type;
use function F2\asserty;

function length_invalidator($value, $type): ?string {
    if (strlen($value) > 10) {
        return "String is too long";
    }
    if (strlen($value) < 5) {
        return "String is too short";
    }
    return null;
}

$type = Type\STRING + [ '#invalidator' => 'length_invalidator' ];

for ($length = 0; $length < 5; $length++) {
    $string = str_pad('', $length, '-');
    asserty( $length < 5 || $length > 10 != Type\is($string, $type) );
}


