<?php
declare(strict_types=1);

namespace F2;

class Type {

    /**
     * @see Type\of()
     */
    public static function of($value): array {
        return Type\of($value);
    }

    /**
     * @see Type\is()
     */
    public static function is($value, array $expectedType): bool {
        return Type\is($value);
    }

    /**
     * @see Type\to()
     */
    public static function to($value, array $targetType) {
        return Type\to($value, $targetType);
    }

}
