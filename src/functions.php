<?php
declare(strict_types=1);

namespace F2\Type;

use TypeError;
use function is_int, is_float, is_bool, is_string, is_resource, is_object, is_callable, is_array, preg_match, is_a, is_subclass_of, method_exists;

/**
 * All the native types and a few special types
 */
const BOOL      = [ '#type' => 'F2\Type\BOOL',      '#name' => 'boolean',   '#F2\Type\BOOL' => true, ];
const INT       = [ '#type' => 'F2\Type\INT',       '#name' => 'integer',   '#F2\Type\INT' => true, ];
const FLOAT     = [ '#type' => 'F2\Type\FLOAT',     '#name' => 'float',     '#F2\Type\FLOAT' => true, ];
const NUMBER    = FLOAT + INT;
const STRING    = [ '#type' => 'F2\Type\STRING',    '#name' => 'string',    '#F2\Type\STRING' => true, ];
const ARR       = [ '#type' => 'F2\Type\ARR',       '#name' => 'array',     '#F2\Type\ARR' => true, ];
const OBJ       = [ '#type' => 'F2\Type\OBJ',       '#name' => 'object',    '#F2\Type\OBJ' => true, ];
const CALLBACK  = [ '#type' => 'F2\Type\CALLBACK',  '#name' => 'callable',  '#F2\Type\CALLBACK' => true, ];
const RESOURCE  = [ '#type' => 'F2\Type\RESOURCE',  '#name' => 'resource',  '#F2\Type\RESOURCE' => true, ];
const NUL       = [ '#type' => 'F2\Type\NUL',       '#name' => 'null',      '#F2\Type\NUL' => true, ];
const TYPE      = [ '#type' => 'F2\Type\TYPE',      '#name' => 'type',      '#F2\Type\TYPE' => true, ];
const CLASSNAME = [ '#type' => 'F2\Type\CLASSNAME', '#name' => 'class',     '#F2\Type\CLASSNAME' => true, ];
const MIXED     = [ '#type' => 'F2\Type\MIXED',     '#name' => 'mixed',     '#F2\Type\MIXED' => true, ] + BOOL + INT + FLOAT + STRING + ARR + OBJ + CALLBACK + RESOURCE + NUL + TYPE + CLASSNAME;
const ITERABLE  = [ '#type' => 'F2\Type\ITERABLE',  '#name' => 'iterator',  '#F2\Type\ITERABLE' => true, ];

/**
 * @example `if (Type\is(123, Type\INT)) { }`
 *
 * Test if a value is of a certain type and that it validates according to validation rules. Multiple types
 * can be combined using the + operator like `Type\is(123, Type\STRING + Type\INT)`.
 *
 * Custom validators:
 *
 * `Type\STRING + [ '#validator' => 'My\Validator::username' ]` or 
 * `Type\STRING + [ '#invalidator' => ['My\Validator::invalidUsername' ]`
 *
 * Integrated validators:
 *
 * [ '#required' => true ]  // value must not be null or "". If not required, then other validators won't run
 * [ '#minval' => 10 ]      // numeric value must be minimum 10
 * [ '#maxval' => 40 ]      // numeric value must be maximum 40
 * [ '#after' => 'B' ]      // value must come after B according to the collator (@see setCollator())
 * [ '#before' => 'E' ]     // value must come before E according to the collator
 * [ '#enum' => [0, 1] ]    // value must be either 0, 1 or 'hello'
 * [ '#minlen' => 1 ],      // string length must be at least 1 (according to mb_strlen())
 * [ '#maxlen' => 10 ],     // string length must be at most 10
 * [ '#instanceof' => User::class ] // value must be an instance of User
 * [ '#instanceof' => [ User::class, Group::class] ] // value must be an instance of User or Group
 * [ '#ctype' => 'alpha' ]  // uses ctype_alpha, ctype_alnum, ctype_digit, ctype_print etc
 * [ '#preg' => '|[a-z]*|'  // uses preg_match
 * [ '#not' => ['', 0]      // value must not be '' or 0
 * [ '#chars' => 'abcde',   // only certain characters allowed
 * [ '#for' => 'email'      // Value must be an e-mail address or url, hostname, ip, ipv4, ipv6, json, 
 *                          // uuid, password
 *
 * Tip! You can define a sted of validators that can be reused by declaring them as a constant, like
 * `const VALID_USERNAME = [ '#ctype' => 'alnum', '#not' => ['root', 'admin'], '#maxlen' => 20 ]`
 *
 * @param mixed $value          The value to test
 * @param array $expectedType   The type to check for
 */
function is($value, array $expectedType): bool {
    _assert_is_type($expectedType);

    /**
     * What is the type of $value?
     */
    $type = of($value);

    /**
     * Compare the received type with the types valid for $expectedType
     */
    if (!isset($expectedType['#'.$type['#type']]) || $expectedType['#'.$type['#type']] !== true) {
        if (!isset($type['#'.$expectedType['#type']]) || $type['#'.$expectedType['#type']] !== true) {
            return false;
        }
    }

    /**
     * If there is an invalidator associated with the type, use it. An invalidator returns
     * null if the value is OK, and a reason if the value is not OK.
     */
    if (isset($expectedType['#invalidator']) && is_callable($expectedType['#invalidator'])) {
        if ($result = $expectedType['#invalidator']($value, $expectedType)) {
            return false;
        }
    }
    /**
     * If there is a validator associated with the type, use it. A validator returns true if
     * the value is valid.
     */
    if (isset($expectedType['#validator']) && is_callable($expectedType['#validator'])) {
        if (!$expectedType['#validator']($value, $expectedType)) {
            return false;
        }
    }

    return true;
}

/**
 * @example `$type = Type\of(123);`
 *
 * Get type of any value. Some values are a combination of types. For example a class name
 * will be returned as a `Type\CLASSNAME + Type\STRING`. A callable string will be returned
 * as `Type\CALLBACK + Type\STRING`. An object that is also callable will be returned as
 * `Type\OBJECT + Type\CALLBACK`.
 *
 * The type returned will not have any of the annotations or validation rules.
 *
 * @param mixed $value  The value you wish to type check
 * @return array        The correct type definition
 */
function of($value): array {
    if ($value === null) {
        return NUL;
    } elseif (is_int($value)) {
        return INT;
    } elseif (is_float($value)) {
        return FLOAT;
    } elseif (is_bool($value)) {
        return BOOL;
    } elseif (is_string($value)) {
        /**
         * Possible optimization, but we're assuming the autoloader
         * is not used for invalid class names and function names
         */
        if (class_exists("\\".$value)) {
            return CLASSNAME + STRING;
        } elseif (is_callable($value)) {
            return CALLBACK + STRING;
        }
        return STRING;
    } elseif (is_array($value)) {
        if (_is_type($value)) {
            return TYPE + ARR;
        } elseif (is_callable($value)) {
            return CALLBACK + ARR;
        }
        return ARR;
    } elseif (is_object($value)) {
        $result = OBJ;
        if (is_callable($value)) {
            $result += CALLBACK;
        }
        if (method_exists($value, 'asString')) {
            $result += STRING;
        }
        if (method_exists($value, 'asInt')) {
            $result += INT + NUMBER;
        }
        if (method_exists($value, 'asFloat')) {
            $result += FLOAT + NUMBER;
        }
        if (method_exists($value, 'asResource')) {
            $result += RESOURCE;
        }
        if (method_exists($value, 'asBool')) {
            $result += BOOL;
        }
        if (method_exists($value, 'asArr') || is_a($value, \ArrayAccess::class)) {
            $result += ARR;
        }
        if (method_exists($value, 'asClassName')) {
            $result += CLASSNAME;
        }
        return $result;
    } elseif (is_callable($value)) {
        return CALLBACK;
    } elseif (is_resource($value)) {
        return RESOURCE;
    } else {
        return MIXED;
    }
}

/**
 * @example `$floatValue = Type\to(123, Type\FLOAT);`
 *
 * Cast a value to a different type, without using the specific cast methods
 * that are available.
 *
 * Tip! This function will not perform validation on the value. If you
 * wish to validate the function, you can use `Type\is($value, Type\INT)`.
 *
 * @param mixed $value      The value you wish to cast to another type
 * @param array $targetType The target type you wish to cast to.
 * @return mixed
 */
function to($value, array $targetType) {
    _assert_is_type($targetType);
    $castFunction = $targetType['#type'];
    return $castFunction($value);
}

/**
 * Cast to a boolean
 */
function bool($value): ?bool {
    if (is_object($value) && method_exists($value, 'asBool')) {
        return BOOL($value->asBool());
    }
    return (bool) $value;
}

/**
 * Cast to an integer
 */
function int($value): ?int {
    if (is_object($value)) {
        if (method_exists($value, 'asInt')) {
            return int($value->asInt());
        }
        if (method_exists($value, 'asFloat')) {
            return int(float($value->asFloat()));
        }
        if (method_exists($value, 'asString') || method_exists($value, '__toString')) {
            return int(string($value->asFloat()));
        }
        return null;
    }
    return (int) $value;
}

/**
 * Cast to a float
 */
function float($value): ?float {
    if (is_object($value)) {
        if (method_exists($value, 'asFloat')) {
            return float($value->asFloat());
        }
        if (method_exists($value, 'asInt')) {
            return float(int($value));
        }
        return null; //float(string($value));
    }
    return (float) $value;
}

/**
 * Cast to a number
 */
function number($value) {
    if (is_object($value)) {
        if (method_exists($value, 'asFloat')) {
            return float($value->asFloat());
        } elseif (method_exists($value, 'asInt')) {
            return int($value->asInt());
        }
        return null;
    }
    if ( (float) $value == (int) $value ) {
        return (int) $value;
    }
    return float($value);
}

/**
 * Cast to a string
 */
function string($value): ?string {
    if (is_array($value)) {
        return null;
    }
    if (is_object($value)) {
        if (method_exists($value, '__toString')) {
            return (string) $value;
        } elseif (method_exists($value, 'asString')) {
            return string($value->asString());
        }
        return null;
    }
    /**
     * Special case handling of types
     */
    if (_is_type($value)) {
        return '{type='.$value['#name'].'}';
    }
    return (string) $value;
}

/**
 * Cast to an array
 */
function arr($value): ?array {
    if (is_array($value)) {
        return $value;
    }
    if (is_object($value)) {
        if (is_a($value, \ArrayAccess::class)) {
            return $value;
        } 
        if (method_exists($value, 'asArr')) {
            return ARR($value->asArr());
        }
    }
    return (array) $value;
}

/**
 * Cast to an object
 */
function obj($value): ?object {
    return (object) $value;

    return $value === null ? null : (object) $value;
}

/**
 * Cast to a callable
 */
function callback($value): ?callable {
    if (is_callable($value)) {
        return $value;
    }
    if (is_object($value) && method_exists($value, 'asCallback')) {
        return callback($value->asCallback());
    }
    return is_callable($value) ? $value : null;
}

/**
 * Cast to a resource
 */
function resource($value) {
    if (is_resource($value)) {
        return $value;
    }
    if (is_object($value) && method_exists($value, 'asResource')) {
        return resource($value->asResource());
    }
    return null;
}

/**
 * Cast to a null value
 */
function nul($value) {
    return null;
}

/**
 * Cast to a mixed value
 */
function mixed($value) {
    return $value;
}

/**
 * Cast to a type
 */
function type($value): ?array {
    if (is_array($value) && isset($value['#type']) && is_string($value['#type']) && constant($value['#type'])) {
        return $value;
    }
    if (is_object($value) && method_exists($value, 'asType')) {
        return type($value->asType());
    }
    return null;
}

/**
 * Cast to a class name
 */
function classname($value): ?string {
    if (is_string($value) && class_exists($value)) {
        return $value;
    }
    if (is_object($value) && method_exists($value, 'asClassName')) {
        return classname($value->asClassName());
    }
    return null;
}

/**
 * Cast to an iterable
 */
function iterable($value): ?iterable {
    if (is_iterable($value)) {
        return $value;
    }
    if (is_object($value) && method_exists($value, 'asIterable')) {
        return iterable($value->asIterable());
    }
    return null;
}

/**
 * Returns true if the provided string is a valid PHP identifier (function names, variable names, namespaces, class names etc)
 */
function _is_identifier(string $value): bool {
    return preg_match('/^[a-zA-Z_\x80-\xff]([\\\\]?[a-zA-Z0-9_\x80-\xff]+)*[a-zA-Z0-9_\x80-\xff]$/', $value);
}

/**
 * Tests if the provided variable is a valid TYPE
 */
function _is_type($value): bool {
/*
echo "_is_type():";
var_dump(type($value));
    $result = type($value) !== null;
var_dump($result);
*/
    return type($value) !== null;
}

function _assert_is_type($value) {
    if (!_is_type($value)) {
        throw new TypeError("Expected a TYPE in argument 2", 1);
    }
}
